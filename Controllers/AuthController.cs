﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Security.Cryptography;
using WeatherApp1.Models;
using System.Data.Common;
using Newtonsoft.Json.Linq;

namespace WeatherApp1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        public IConfiguration Configuration { get; }

        public AuthController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpPost]
        [Route("register")]
        public IActionResult Register(User user)
        {
            string connString = this.Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection connection = new SqlConnection(connString))
            {
                string encryptPassword = this.Encrypt(user.Password);
                string sql = $"insert into Users (Username, Password) values ('{user.Username}', '{encryptPassword}')";

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = sql;
                cmd.Connection = connection;

                connection.Open();
                cmd.ExecuteNonQuery(); 
                connection.Close();
            }
            return Ok(new
            {
                success = true,
                username = user.Username
            });
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login(User user)
        {
            string connString = this.Configuration.GetConnectionString("DefaultConnection");
            
            using (SqlConnection connection = new SqlConnection(connString))
            {
                string encryptPassword = this.Encrypt(user.Password);
                string sql = $"select Username, Password from Users where Username = '{user.Username}'";

                SqlCommand cmd = new SqlCommand(sql, connection);

                connection.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        if (this.Encrypt(user.Password) == reader["Password"].ToString())
                        {
                            var key = Encoding.ASCII.GetBytes(this.Configuration["JWT:Key"]);

                            var tokenDescriptor = new SecurityTokenDescriptor
                            {
                                Subject = new ClaimsIdentity(new[]
                                {
                                    new Claim(ClaimTypes.Name, user.Username)
                                }),
                                Expires = DateTime.UtcNow.AddMinutes(7),
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
                            };

                            var tokenHandler = new JwtSecurityTokenHandler();
                            var token = tokenHandler.CreateToken(tokenDescriptor);
                            var jwtToken = tokenHandler.WriteToken(token);

                            connection.Close();

                            return Ok(new
                            {
                                success = true,
                                expiresIn = DateTime.UtcNow.AddMinutes(7),
                                token = jwtToken
                            });
                        } else
                        {
                            return BadRequest(new 
                            { 
                                success = false, message = "Wrong password"
                            });
                        }
                        
                    }
                    return BadRequest(new
                    {
                        success = false,
                        message = "Wrong username/email"
                    });
                }
                
            }
       
        }

        [Authorize]
        [HttpPost]
        [Route("logout")]
        public IActionResult Logout([FromBody] string username, string password)
        {

            return Ok();
        }

        private string Encrypt(string input)
        {
            var bytes = Encoding.UTF8.GetBytes(input);
            using (var hash = SHA512.Create())
            {
                var hashedBytes = hash.ComputeHash(bytes);
                var hashedInputStringBuilder = new StringBuilder(128);
                foreach (var b in hashedBytes)
                {
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                }
                return hashedInputStringBuilder.ToString();
            }
        }
    }
}
