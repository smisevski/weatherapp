using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using WeatherApp1.Models;

namespace WeatherApp1.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [Authorize]
        [HttpPost]
        [Route("search")]
        public async Task<IActionResult> Search(string Keyword) 
        {
            HttpClient httpClient = new HttpClient();
            string apiRoute = $"http://api.openweathermap.org/data/2.5/forecast?q={Keyword}&cnt=3&appid=348f7709abca894a1f68d4f1101ea74a";

            using HttpResponseMessage response = await httpClient.GetAsync(apiRoute);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            return Ok(jsonResponse);
        }
    }
}