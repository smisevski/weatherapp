# WeatherApp

## Installation and Testing

### The WebAPI
	1. Clone the repository 
	2. Open the solution with Visual Studio
	3. Create a database as per the given connection string in appsettings.json
	4. Run the application -> check hostname and port

### The Angular ClientApp
	1. Get inside the root of ClientApp directory in terminal
	2. Execute "npm i" and verify node_modules install
	3. Execute "ng serve" to run the ClientApp
	4. Check if hostname and port are coresponding to the WebAPI information in user.service.ts and weather.service.ts

### If everything okay
	1. "/" Route is empty component
	2. "/login", "/register" - visit manually to test user authentication
	3. successfull login should redirect to "/weather" route which is guarded
	4. "/weather" route contains the search form input which supports original city name input (eg. Skopje, Belgrade, Athens)

#### NOTES:
	* Database table for dbo.Users was manually created in MS SqlServer
	* Initial database was created in MS SqlServer, but the connection string can be changed to suite MariaDB/MySql

#### PS:
	* The temperatures min and max are displayed in Kelvin units as from the API format, should find template variable formatting to Celsius
	* The confirmValidate method in register.component.ts should be provided as an async validator for Password and confirmPassword fields