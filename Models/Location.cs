﻿using System.ComponentModel.DataAnnotations;

namespace WeatherApp1.Models
{
    public class Location
    {
        public decimal id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public Object coord { get; set; }
    }
}
