import { Injectable } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}) export class UserService {
  private httpService: HttpService;
  constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public register(data: FormGroup): Observable<any> {
    return this.httpService.call('POST', 'https://localhost:7256/Auth/register', { responseType: 'json', data });
  }

  public login(data: FormGroup): Observable<any> {
    return this.httpService.call('POST', 'https://localhost:7256/Auth/login', { responseType: 'json', data });
  }
}
