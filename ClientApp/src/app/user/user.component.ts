import { Component, Input, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from '../localStorage.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [ LocalStorageService, UserService]
})
export class UserComponent implements OnInit {
  @Input() Type!: string;
  formData: FormGroup;

  public constructor(private localStorageService: LocalStorageService,private userService: UserService, private router: Router) {
    this.formData = new FormGroup({});
  }

  ngOnInit() {
    this.formData = new FormGroup(
      {
        Username: new FormControl(null, [Validators.required, Validators.email]),
        Password: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(16)]),
      }
    )
  }

  submitLogin() {
    if (this.formData.valid) {
      let response = this.userService.login(this.formData.value);
      response.subscribe((data) => {
        if (data.success) {
          this.localStorageService.setWithExpiry('accessToken', data.token, data.expiresIn);
          this.router.navigate(['/weather']);
        } else {
          console.error(data.message)
        }
      });
    }
  }
}

