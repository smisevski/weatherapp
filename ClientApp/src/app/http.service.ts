import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService } from './localStorage.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  httpClient: HttpClient;
  constructor(httpClient: HttpClient, private localStorageService: LocalStorageService) {
    this.httpClient = httpClient;
  }

  public call(method: string, url: string, bodyData: any): Observable<any> {
    // let httpHeaders: HttpHeaders = new HttpHeaders();
    let token = this.localStorageService.getWithExpiry('accessToken');
    let authHeader = `Bearer ${token}`;
    let headers = { 'Authorization': authHeader }
    let options = {
      headers: headers,
      body: bodyData.data
    }
    const req = this.httpClient.request(method, url, options);
    return req;
  }
}
