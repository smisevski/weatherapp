import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  set(key: string, value: Object) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    get(key: string) {
        return localStorage.getItem(key);
    }

    remove(key: string) {
        localStorage.removeItem(key);
    }

    setWithExpiry(key: string, value: string, ttl: number) {
        const now = new Date();
    
        const item = {
          value: value,
          expiry: now.getTime() + ttl,
        };
        localStorage.setItem(key, JSON.stringify(item));
      }

      getWithExpiry(key: string) {
        const itemStr = localStorage.getItem(key)

        if (!itemStr) {
            return null;
        }
        const item = JSON.parse(itemStr);
        const now = new Date();
        if (now.getTime() > item.expiry) {
            localStorage.removeItem(key);
            return null;
        }
        return item.value;
    }
}
