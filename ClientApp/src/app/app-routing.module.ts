import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegisterComponent } from './register/register.component';
import { WeatherComponent } from './weather/weather.component';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  {
    path: 'login',
    component: UserComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'weather',
    component: WeatherComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
