import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageService } from './localStorage.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard {
  constructor(private router: Router, private localStorageService: LocalStorageService) {};

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let accessToken = this.localStorageService.getWithExpiry("accessToken");
        if (accessToken !== null) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }   
  
}
