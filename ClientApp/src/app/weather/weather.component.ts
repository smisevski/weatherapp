import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WeatherService } from '../weather.service';
import { LocalStorageService } from '../localStorage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  formData: FormGroup;
  days: any;
  /**
   *
   */
  constructor(private weatherService: WeatherService, private localStorageService: LocalStorageService, private router: Router) { 
    this.formData = new FormGroup({});
    this.days = [];
   }

  ngOnInit(): void {
    this.formData = new FormGroup(
      {
        Keyword: new FormControl(null, [Validators.required]),
      }
    )
  }

  search() {
    if (this.formData.valid) {
      this.weatherService.search(this.formData.value.Keyword).subscribe((data) => {
        if (data) {
          this.days = data.list;
        } else {
          console.error(data.message)
        }
      })
    }
  }

  logout() {
    this.localStorageService.remove('accessToken');
    this.router.navigate(['/login']);
  }
}
