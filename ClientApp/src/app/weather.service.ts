import { Injectable } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
}) export class WeatherService {
  private httpService: HttpService;
  constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public search(keyword: string): Observable<any> {
    let url = `https://localhost:7256/WeatherForecast/search?Keyword=${keyword}`
    return this.httpService.call('POST', url, { responseType: 'json' });
  }

}