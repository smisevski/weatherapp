/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { LocalStorageService } from '../localStorage.service';
import { UserService } from '../user.service';

function validatePasswordCompare(control: string, password: string) {
  console.log("validate: ", control + " - " + password)
  return of(control !== password).pipe(delay(1000));
};

function confirmedValidator(controlName: string, matchingControlName: string) {
  return (formGroup: any) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (
      matchingControl.errors &&
      !matchingControl.errors.confirmedValidator
    ) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ confirmedValidator: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export class CustomValidator {
  static createValidator(controlName: string): AsyncValidatorFn {
    return (control: AbstractControl): Observable<any> => {
      return validatePasswordCompare(control.value, controlName).pipe(
        map((result: boolean) =>
          result ? { passwordConfirmed: true } : null
        )
      );
    };
  }
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [LocalStorageService, UserService]
})

export class RegisterComponent implements OnInit {
  formData: FormGroup;
  isAgreed: boolean;
  pass: string;
  
  constructor(private localStorageService: LocalStorageService, private userService: UserService, private router: Router, private fb: FormBuilder) {
    this.formData = new FormGroup({});
    this.pass = "";
    this.isAgreed = false;
  }
  ngOnInit() {
    this.formData = new FormGroup({
      /*na confirmEmail async validator za sporedba so email
        na email
      */
      Username: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      Password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(16)])),
      confirmPassword: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(16)])),
      termsAgreements: new FormControl(false, Validators.requiredTrue),
    });
  }

  updateTA(flag: boolean) {
    this.isAgreed = flag;
  }

  submitRegister() {
    if (this.formData.valid) {
      let response = this.userService.register(this.formData.value);
      response.subscribe((data) => {
        if (data.success) {
          this.localStorageService.setWithExpiry('accessToken', data.token, data.expiresIn);
          this.router.navigate(['/weather']);
        } else {
          console.error(data.message)
        }
      });
    }
  }

  setPassword() {
    this.pass = this.formData.value.Password;
  }

  confirmedValidator(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (
        matchingControl.errors &&
        !matchingControl.errors.confirmedValidator
      ) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
}
